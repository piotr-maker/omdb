#ifndef MODEL_MOVIE_H_
#define MODEL_MOVIE_H_

#include <string>
#include <vector>
#include <nlohmann/json.hpp>

struct Rating
{
    std::string source;
    std::string value;

    friend void from_json(const nlohmann::json&, Rating&);
};

struct Movie
{
    std::string year;
    std::string released;
    std::string imdbRating;
    std::string title;
    std::string rated;
    std::string runtime;
    std::string genre;
    std::string director;
    std::string writer;
    std::string actors;
    std::string description;
    std::string language;
    std::string country;
    std::string awards;
    std::string poster;
    std::vector<Rating> ratings;

    friend void from_json(const nlohmann::json&, Movie&);
};

#endif /* MODEL_MOVIE_H_ */
