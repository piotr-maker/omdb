#include <cstdlib>
#include <iostream>
#include <config.h>
#include <Movie.h>
#include <nlohmann/json.hpp>
#include <restclient-cpp/connection.h>
#include <restclient-cpp/restclient.h>
#include "config.h.in"
#include "extern/popl/include/popl.hpp"

namespace RC = RestClient;

const char* apiKey = "db4f91b9";

int main(int argc, char* argv[])
{
    popl::OptionParser op("Usage");
    auto help_op = op.add<popl::Switch>("h", "help", "produce help message");
    auto title_op = op.add<popl::Value<std::string>>("t", "title", "movie title");
    auto version_op = op.add<popl::Switch>("v", "version", "program version");
    op.parse(argc, argv);

    if(version_op->is_set())
    {
        std::cout << "Version: " << PROJECT_VER << '\n';
        return EXIT_SUCCESS;
    }

    if(!op.unknown_options().empty())
    {
        std::cout << "Unknown option: " << op.unknown_options()[0] << '\n';
        std::cout << op.help() << '\n';
        return EXIT_FAILURE;
    }

    if(!op.non_option_args().empty())
    {
        std::cout << "Unknown argument: " << op.non_option_args()[0] << '\n';
        std::cout << op.help() << '\n';
        return EXIT_FAILURE;
    }

    if(help_op->is_set() || !title_op->is_set())
    {
        std::cout << op.help() << '\n';
        return EXIT_SUCCESS;
    }

    std::string query = "&t=" + std::string(title_op->value());
    std::string url = "http://www.omdbapi.com/?apikey=" + std::string(apiKey);
    RC::init();
    RC::Connection conn(url);
    RC::HeaderFields headers;
    headers["Accept"] = "application/json";
    conn.SetHeaders(headers);
    std::cout << "Sending: " << url << query << '\n';
    RC::Response response = conn.get(query);
    RC::disable();
    if(response.code != 200)
    {
        std::cout << "Error: " << response.body << '\n';
        return EXIT_FAILURE;
    }

    Movie movie;
    try {
        nlohmann::json json = nlohmann::json::parse(response.body);
        movie = json.get<Movie>();
    } catch (const nlohmann::json::exception& ex) {
        std::cout << "Parse error: " << ex.what() << '\n';
        return EXIT_FAILURE;
    }

    std::cout << "Title: " << movie.title << '\n';
    std::cout << "Runtime: " << movie.runtime << '\n';
    std::cout << "Year: " << movie.year << '\n';
    std::cout << "Genre: " << movie.genre << '\n';
    std::cout << "Director: " << movie.director << '\n';
    std::cout << "Writer: " << movie.writer << '\n';
    std::cout << "Actors: " << movie.actors << '\n';
    std::cout << "Language: " << movie.language << '\n';
    std::cout << "Country: " << movie.country << '\n';
    std::cout << "Ratings: " << '\n';
    for(auto rating : movie.ratings)
        std::cout << '\t' << rating.source << ": " << rating.value << '\n';
    std::cout << "Description: " << movie.description << '\n';
    return EXIT_SUCCESS;
}
