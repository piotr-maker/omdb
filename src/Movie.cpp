#include "Movie.h"

void from_json(const nlohmann::json& json, Rating& rating)
{
    json.at("Source").get_to(rating.source );
    json.at("Value").get_to(rating.value );
}

void from_json(const nlohmann::json& json, Movie& movie)
{
    json.at("Year").get_to(movie.year );
    json.at("Released").get_to(movie.released);
    json.at("imdbRating").get_to(movie.imdbRating);
    json.at("Title").get_to(movie.title);
    json.at("Rated").get_to(movie.rated);
    json.at("Runtime").get_to(movie.runtime);
    json.at("Genre").get_to(movie.genre );
    json.at("Director").get_to(movie.director );
    json.at("Writer").get_to(movie.writer );
    json.at("Actors").get_to(movie.actors );
    json.at("Plot").get_to(movie.description );
    json.at("Language").get_to(movie.language );
    json.at("Country").get_to(movie.country );
    json.at("Awards").get_to(movie.awards );
    json.at("Poster").get_to(movie.poster );
    json.at("Ratings").get_to(movie.ratings);
}
